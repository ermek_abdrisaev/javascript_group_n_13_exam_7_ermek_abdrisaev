import {Component, EventEmitter, Input, Output} from '@angular/core';
import {FoodAndDrink} from "../shared/FD.modal";
import {Order} from "../shared/order.modal";


@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent{
  @Input() array!: Order;
  @Input() arrayOrders: Order[] = [];

  @Output() newFoodAndDrink = new EventEmitter<FoodAndDrink>();


  getPrice(){
    return this.arrayOrders.reduce((acc, Order) =>{
      return acc + Order.price;
    },0);
  }


}
