export class Order {
  constructor(
    public name: string,
    public count: number,
    public price: number,
    public imageUrl: string,
  ){}
}
