export class FoodAndDrink {
  constructor(
    public name: string,
    public count: number,
    public price: number,
    public imageUrl: string,
  ){}
}
