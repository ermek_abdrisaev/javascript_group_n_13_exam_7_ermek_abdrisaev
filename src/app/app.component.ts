import { Component } from '@angular/core';
import { FoodAndDrink } from "./shared/FD.modal";
import {Order} from "./shared/order.modal";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  arrayOrders: Order[] = [];


  foodAndDrinks: FoodAndDrink[] = [
    new FoodAndDrink('Coke', 0,50, "https://s.abcnews.com/images/US/new-coke-ht-jpo-190521_hpEmbed_9x13_992.jpg"),
    new FoodAndDrink('Pepsi', 0, 50, "https://st2.depositphotos.com/1022135/5706/i/600/depositphotos_57066805-stock-photo-pepsi-can.jpg"),
    new FoodAndDrink('Shaverma', 0,120, "https://croc29.ru/image/cache/catalog/new-poster/shaurma-900x900.jpg"),
    new FoodAndDrink('Samsa',0, 50, "https://beeyor.tj/wp-content/uploads/2020/10/20.svg"),
    new FoodAndDrink('Sendwich', 0, 80, "https://static6.depositphotos.com/1066961/552/i/600/depositphotos_5526915-stock-photo-grilled-panini.jpg"),
    new FoodAndDrink('Black tea', 0,20, "https://files.dutchbros.com/website/menu/drink-images-v2/512px2/Iced_Double_Rainbro_Tea_HERO.png"),
  ];

  deleteOrder() {

  }

  onClickOrder(index: number){
    console.log(this.foodAndDrinks[index]);
    this.arrayOrders.push(this.foodAndDrinks[index]);
    console.log(this.arrayOrders);
  }




}
