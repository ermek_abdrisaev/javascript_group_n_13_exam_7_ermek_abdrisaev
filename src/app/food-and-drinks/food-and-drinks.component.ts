import {Component, EventEmitter, Input, Output} from '@angular/core';
import {FoodAndDrink} from "../shared/FD.modal";

@Component({
  selector: 'app-food-and-drinks',
  templateUrl: './food-and-drinks.component.html',
  styleUrls: ['./food-and-drinks.component.css']
})
export class FoodAndDrinksComponent  {
  @Output() add = new EventEmitter();

  @Input() array!: FoodAndDrink;

  onClick(){
    this.add.emit();
  }
}
